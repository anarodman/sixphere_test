﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using API.Models;

namespace API.Controllers
{
    [ApiController]
    [Route("api/orders")]
    public class OrdersController : ControllerBase
    {
        private readonly OrdersContext _dbContext;

        private readonly ILogger<OrdersController> _logger;

        public OrdersController(OrdersContext dbContext, ILogger<OrdersController> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        [HttpGet("")]
        public IEnumerable<Order> Get()
        {
            return _dbContext.Orders
                    .Include(order => order.Operations)
                    .ToArray();
        }

        [HttpGet("{id:int}/operations")]
        public IEnumerable<Operation> Get(int id){

            return _dbContext.Operations
                    .Where(operation => operation.OrderId.Equals(id))
                    .ToArray();
        }
    }
}
