using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using API.Models;

namespace API.Controllers
{
    [ApiController]
    [Route("api/operations")]
    public class OperationsController : ControllerBase
    {
        private readonly OrdersContext _dbContext;

        private readonly ILogger<OperationsController> _logger;

        public OperationsController(OrdersContext dbContext, ILogger<OperationsController> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }
        
        [HttpGet]
        public IEnumerable<Operation> Get(){

            return _dbContext.Operations
                    .ToArray();
            /*return _dbContext.Operations
                    .Include(operation => operation.OrderId.Equals(1))
                    .ToArray();*/
        }
        
        
        /*[HttpGet]
        public IEnumerable<Operation> Get(int id){

            return _dbContext.Operations
                    .Include(operation => operation.OrderId.Equals(id))
                    .ToArray();
        }*/
        
    }
}
