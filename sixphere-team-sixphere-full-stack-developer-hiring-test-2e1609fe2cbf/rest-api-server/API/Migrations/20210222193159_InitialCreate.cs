﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using MySql.EntityFrameworkCore.Metadata;

namespace API.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "orders",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "text", nullable: false),
                    description = table.Column<string>(type: "text", nullable: true),
                    due_date = table.Column<DateTime>(type: "datetime", nullable: true),
                    created_at = table.Column<DateTime>(type: "datetime", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    updated_at = table.Column<DateTime>(type: "datetime", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.ComputedColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_orders", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "operations",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "text", nullable: false),
                    description = table.Column<string>(type: "text", nullable: true),
                    order_id = table.Column<int>(type: "int", nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    updated_at = table.Column<DateTime>(type: "datetime", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.ComputedColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_operations", x => x.id);
                    table.ForeignKey(
                        name: "FK_operations_orders_order_id",
                        column: x => x.order_id,
                        principalTable: "orders",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "orders",
                columns: new[] { "id", "code", "description", "due_date" },
                values: new object[,]
                {
                    { 1, "Order-1", "This a description", null },
                    { 2, "Order-2", "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", null },
                    { 3, "Order-3", "Donec posuere mattis erat quis iaculis.", null },
                    { 4, "Order-4", "Cras accumsan sed nibh vitae aliquet.", null },
                    { 5, "Order-5", "Fusce accumsan, massa porttitor placerat convallis.", null }
                });

            migrationBuilder.InsertData(
                table: "operations",
                columns: new[] { "id", "code", "description", "order_id" },
                values: new object[,]
                {
                    { 1, "Operation-1-1", "Fusce accumsan, massa porttitor placerat convallis.", 1 },
                    { 2, "Operation-1-2", "Donec posuere mattis erat quis iaculis.", 1 },
                    { 3, "Operation-1-3", "Donec posuere mattis erat quis iaculis.", 1 },
                    { 4, "Operation-1-4", "Cras accumsan sed nibh vitae aliquet.", 1 },
                    { 5, "Operation-2-1", "Cras accumsan sed nibh vitae aliquet.", 2 },
                    { 6, "Operation-2-2", "Cras accumsan sed nibh vitae aliquet.", 2 },
                    { 7, "Operation-2-3", "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", 3 },
                    { 8, "Operation-2-4", "Fusce accumsan, massa porttitor placerat convallis.", 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_operations_order_id",
                table: "operations",
                column: "order_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "operations");

            migrationBuilder.DropTable(
                name: "orders");
        }
    }
}
