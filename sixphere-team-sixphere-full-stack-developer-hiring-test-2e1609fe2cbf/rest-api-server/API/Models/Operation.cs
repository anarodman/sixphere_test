using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models
{
    [Table("operations")]
    public class Operation
    {
        [Column("id")]
        [Key]
        public int Id { get; set; }
        
        [Column("code")]
        [Required]
        public string Code { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("order_id")]
        [Required]
        public int OrderId {get; set; }
        
        [Column("created_at")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreatedAt { get; set; }

        [Column("updated_at")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime UpdatedAt { get; set; }
    }
}
