using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models
{
    [Table("orders")]
    public class Order
    {
        [Column("id")]
        [Key]
        public int Id { get; set; }
        
        [Column("code")]
        [Required]
        public string Code { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("due_date")]
        public DateTime? DueDate { get; set; }

        public ICollection<Operation> Operations { get; set; }

        [Column("created_at")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreatedAt { get; set; }

        [Column("updated_at")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime UpdatedAt { get; set; }
    }
}
