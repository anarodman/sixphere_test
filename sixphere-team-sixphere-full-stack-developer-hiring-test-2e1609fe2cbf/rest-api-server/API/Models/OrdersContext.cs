using Microsoft.EntityFrameworkCore;

namespace API.Models
{
    public class OrdersContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }
        public DbSet<Operation> Operations { get; set; }

        public OrdersContext(DbContextOptions<OrdersContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder) {

            // DEFAULT ORDER DATA
            builder.Entity<Order>().HasData(new Order { 
                Id = 1,
                Code = "Order-1",
                Description = "This a description"
            });
            builder.Entity<Order>().HasData(new Order { 
                Id = 2,
                Code = "Order-2",
                Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
            });
            builder.Entity<Order>().HasData(new Order { 
                Id = 3,
                Code = "Order-3",
                Description = "Donec posuere mattis erat quis iaculis."
            });
            builder.Entity<Order>().HasData(new Order { 
                Id = 4,
                Code = "Order-4",
                Description = "Cras accumsan sed nibh vitae aliquet."
            });
            builder.Entity<Order>().HasData(new Order { 
                Id = 5,
                Code = "Order-5",
                Description = "Fusce accumsan, massa porttitor placerat convallis."
            });

            // DEFAULT OPERATION DATA
            builder.Entity<Operation>().HasData(new Operation { 
                Id = 1,
                Code = "Operation-1-1",
                Description = "Fusce accumsan, massa porttitor placerat convallis.",
                OrderId = 1
            });
            builder.Entity<Operation>().HasData(new Operation { 
                Id = 2,
                Code = "Operation-1-2",
                Description = "Donec posuere mattis erat quis iaculis.",
                OrderId = 1
            });
            builder.Entity<Operation>().HasData(new Operation { 
                Id = 3,
                Code = "Operation-1-3",
                Description = "Donec posuere mattis erat quis iaculis.",
                OrderId = 1
            });
            builder.Entity<Operation>().HasData(new Operation { 
                Id = 4,
                Code = "Operation-1-4",
                Description = "Cras accumsan sed nibh vitae aliquet.",
                OrderId = 1
            });

            builder.Entity<Operation>().HasData(new Operation { 
                Id = 5,
                Code = "Operation-2-1",
                Description = "Cras accumsan sed nibh vitae aliquet.",
                OrderId = 2
            });
            builder.Entity<Operation>().HasData(new Operation { 
                Id = 6,
                Code = "Operation-2-2",
                Description = "Cras accumsan sed nibh vitae aliquet.",
                OrderId = 2
            });
            builder.Entity<Operation>().HasData(new Operation { 
                Id = 7,
                Code = "Operation-2-3",
                Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                OrderId = 3
            });
            builder.Entity<Operation>().HasData(new Operation { 
                Id = 8,
                Code = "Operation-2-4",
                Description = "Fusce accumsan, massa porttitor placerat convallis.",
                OrderId = 3
            });



            base.OnModelCreating(builder);
        }
    }
}