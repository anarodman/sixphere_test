<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use GuzzleHttp\Client;

class Order
{
    use HasFactory;
    protected static $url = "http://rest-api-server:5000/api/orders/";

    public static function all()
    {
        $client = new Client();
        $response = $client->request('GET', self::$url, [
            'verify'  => false,
        ]);
        return json_decode($response->getBody());
    }

    public static function get($id)
    {
        $client = new Client();
        $response = $client->request('GET', self::$url . $id, [
            'verify'  => false,
        ]);
        return json_decode($response->getBody());
    }
}
