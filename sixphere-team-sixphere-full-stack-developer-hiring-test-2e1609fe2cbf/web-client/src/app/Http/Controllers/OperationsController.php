<?php

namespace App\Http\Controllers;

use App\Models\Operation;
use Illuminate\Http\Request;

class OperationsController extends Controller
{

    /**
     * Index view
     *
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $operations = Operation::all();
            return response()->json($operations);
        }

        return view('operations.index');
    }

    public function get($id) {
        if (Operation::get('id', $id)->exists()) {
            $operations = Operation::get('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response(operations, 200);
          } else {
            return response()->json([
              "message" => "Operation not found"
            ], 404);
          }
      }
}
