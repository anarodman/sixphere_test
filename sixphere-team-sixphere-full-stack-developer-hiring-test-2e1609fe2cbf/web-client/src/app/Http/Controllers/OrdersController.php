<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{

    /**
     * Index view
     *
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $orders = Order::all();
            return response()->json($orders);
        }

        return view('orders.index');
    }

    public function get($id) {
        if (Operation::get('id', $id)->exists()) {
            $operations = Operation::where('order_id', $id);
            return response()->json($operations);
          } else {
            return response()->json([
              "message" => "Operation not found"
            ], 404);
          }
          return view('operations.index');
      }

    
}
