import Vue from 'vue';

import OrdersList from './components/OrdersList.vue';
import OperationsList from './components/OperationsList.vue';

Vue.component('order-list', OrdersList);
Vue.component('operation-list', OperationsList);

new Vue({
    el: '#app',

    data() {
        return {

        };
    },
});
