import BaseService from "./BaseService";

class OperationService extends BaseService {

    async get(): Promise<any> {
        return this._http.get('/operations').then((response) => {
            return response.data;
        }).catch((e) => {
            // @ts-ignore
            Swal.fire({
                title: 'Error',
                text: 'No hemos podido obtener las operaciones.',
                icon: 'error',
                confirmButtonText: 'Ok'
              })

        })
    }
}

export const operationService = new OperationService();
