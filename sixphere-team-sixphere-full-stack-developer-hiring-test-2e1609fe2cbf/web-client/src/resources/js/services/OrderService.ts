import BaseService from "./BaseService";

class OrderService extends BaseService {

    async get(): Promise<any> {
        return this._http.get('/orders').then((response) => {
            return response.data;
        }).catch((e) => {
            // @ts-ignore
            Swal.fire({
                title: 'Error',
                text: 'No hemos podido obtener las órdenes.',
                icon: 'error',
                confirmButtonText: 'Ok'
              })

        })
    }
}

export const orderService = new OrderService();
