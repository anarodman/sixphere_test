import axios from 'axios'
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';


export default class BaseService {
    // @ts-ignore
    _http = axios.create({
        baseURL: ``,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    });
    constructor(){
    }
};
