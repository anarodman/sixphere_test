<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Sixphere - Vue + Laravel + .Net Core</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" href="https://sixphere.com/wp-content/uploads/2020/05/cropped-logo_sixphere-32x32.png" sizes="32x32">
        <link rel="icon" href="https://sixphere.com/wp-content/uploads/2020/05/cropped-logo_sixphere-192x192.png" sizes="192x192">
        <link rel="apple-touch-icon" href="https://sixphere.com/wp-content/uploads/2020/05/cropped-logo_sixphere-180x180.png">
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    </head>
    <body class="bg-gray-100">
        <div class="container mx-auto">
        @include('navbar')
        @yield('main-content')
        <script src="{{asset('js/app.js')}}"></script>
        </div>
        @include('footer')
    </body>
</html>
