<div class="relative bg-white">
    <div class="max-w-8xl mx-auto px-4  sm:px-6">
        <div class="flex justify-between items-center py-6 md:justify-start md:space-x-10">
            <div class="flex justify-start lg:w-0 lg:flex-1">
                <a href="https://www.sixphere.com">
                    <span class="sr-only">Sixphere</span>
                    <img src="https://sixphere.com/wp-content/uploads/2021/02/logo-min-orange.png" alt="Sixphere" class="h-8 w-auto sm:h-10" />
                </a>
            </div>
            <nav class="md:flex space-x-10 items-center justify-end md:flex-1 lg:w-0" >
                <a href="/orders" class="text-base font-medium text-gray-500 hover:text-gray-900">
                    Órdenes
                </a>
                <a href="/" class="text-base font-medium text-gray-500 hover:text-gray-900">
                    Documentación
                </a>
            </nav>
        </div>
    </div>
</div>
