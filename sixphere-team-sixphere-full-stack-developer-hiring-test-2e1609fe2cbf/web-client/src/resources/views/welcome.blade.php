@extends('layout')

@section('main-content')
<div class="w-full mt-8">
    <div class="grid grid-cols-1 gap-y-6 lg:grid-cols-5 lg:grid-flow-col lg:gap-8">
        <div class="mx-auto">
            <a href="https://www.sixphere.com">
                <img src="./images/sixphere_banner.png" alt="Sixphere">
            </a>
        </div>
        <div class="col-span-4 flex items-center justify-center">
            <div style="color: #543855;" class="text-center">
                <h1 class="text-8xl font-bold" >
                    Prueba técnica
                </h1>
                <h3 class="text-6xl">Vue + Laravel + .Net Core</h2>
                    <h6 class="mt-16 text-3xl font-semibold text-right" style="color: #543855;">
                        <span style="color: #ffa327;">Sixphere</span>
                    </h6>
                </div>
            </div>
        </div>
        <div>
            <div class="mt-16">
                <div class="sm:flex sm:items-baseline sm:justify-between">
                    <h2 id="getting-started" class="text-2xl font-semibold tracking-tight font-display text-gray-900 sm:text-3xl">
                        ¿Cómo empezar?
                    </h2>
                </div>
            </div>
            <div class="border-t border-gray-200 mt-8 pt-8 grid grid-cols-1 gap-y-6 lg:grid-cols-5 lg:gap-5">
                <div>
                    <h3 class="text-lg font-medium tracking-tight text-gray-900">
                        Requisitos
                    </h3>
                </div>
                <div class="col-span-4">
                    <div class="">
                        <p>
                            Antes de comenzar esta prueba, es necesario que te asegures de que tu sistema tiene instaladas las siguientes
                            aplicaciones:
                        </p>
                        <ul class="ml-4 list-inside list-disc">
                            <li>
                                <a  class="text-yellow-500" href="https://docs.docker.com/get-docker/">Docker</a>
                            </li>
                            <li>
                                <a  class="text-yellow-500" href="https://docs.docker.com/compose/install/">Docker Compose</a>
                            </li>
                            <li>
                                <a  class="text-yellow-500" href="https://git-scm.com/downloads">Git</a>
                            </li>
                            <li>
                                <a  class="text-yellow-500" href="https://github.com/petervanderdoes/gitflow-avh/wiki/Installation">Git Flow</a>
                            </li>
                        </ul>
                        <br>

                    </div>
                </div>
            </div>
            <div class="border-t border-gray-200 mt-8 pt-8 grid grid-cols-1 gap-y-6 lg:grid-cols-5 lg:gap-5">
                <div>
                    <h3 class="text-lg font-medium tracking-tight text-gray-900">
                        Recomendaciones
                    </h3>
                </div>
                <div class="col-span-4">
                    <div class="">
                        <p>
                            Aunque puedes desarrollar en el IDE en el que más te guste, nosotros te recomendamos utilizar <a class="text-yellow-500" href="https://code.visualstudio.com/Download" title="Descarga VSC">Visual Studio Code</a>
                            con algunos paquetes de desarrollo remoto que te ayudarán a implementar los ejercicios de manera más efectiva:
                        </p>
                        <ul class="ml-4 list-inside list-disc">
                            <li>
                                <a class="text-yellow-500" href="https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack">Remote Development</a>
                                - Una extensión que te permite abrir cualquier carpeta en un contenedor o en una máquina remota aprovechando todo el conjunto de funciones de VS Code.
                            </li>
                            <li>
                                <a class="text-yellow-500" href="https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare">Live Share</a>
                                - Herramienta de desarrollo colaborativo en tiempo real.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="border-t border-gray-200 mt-8 pt-8 grid grid-cols-1 gap-y-6 lg:grid-cols-5 lg:gap-5">
                <div>
                    <h3 class="text-lg font-medium tracking-tight text-gray-900">
                        Código
                    </h3>
                </div>
                <div class="col-span-4">
                    <div class="">
                        <p>
                            Lo tienes a tu disposición en nuestro <a class="text-yellow-500" href="https://bitbucket.org/sixphere-team/sixphere-full-stack-developer-hiring-test/">repositorio</a>.
                        </p>
                        <p>
                            Clona el repositorio y comienza a trabajar sobre ello.
                        </p>
                    </div>
                </div>
            </div>
            <div class="border-t border-gray-200 mt-8 pt-8 grid grid-cols-1 gap-y-6 lg:grid-cols-5 lg:gap-5">
                <div>
                    <h3 class="text-lg font-medium tracking-tight text-gray-900">
                        Arranca el proyecto
                    </h3>
                </div>
                <div class="col-span-4">
                    <div class="">
                        <p>
                            En el directorio raíz del proyecto se encuentra un arhivo <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">docker-compose.yml</span>
                            que contiene la definición de los servicios <span class="italic">docker</span> necesarios.
                            Para arrancar el proyecto necesitas lanzar el siguiente comando:
                        </p>
                        <div class="mx-auto my-4 p-4 bg-white text-gray-500 border-b font-mono border-t border-gray-200 sm:border sm:rounded-lg overflow-hidden">
                            <div class="my-0 leading-none">docker-compose up -d</div>
                        </div>
                        <p>
                            Después de levantar los 3 servicios, comprueba que éstos están operativos con el siguiente comando:
                        </p>
                        <div class="mx-auto my-4 p-4 bg-white text-gray-500 border-b font-mono border-t border-gray-200 sm:border sm:rounded-lg overflow-hidden">
                            <div class="my-0 leading-none">docker-compose ps</div>
                        </div>
                        <p>
                            Si todo está funcionando correctamente. ¡Puedes empezar a desarrollar!
                        </p>
                    </div>
                </div>
            </div>
            <div class="rounded bg-purple-200 p-4 mt-8">
                <div class="text-sm text-purple-500 flex items-center">
                    <div class="mr-4 rounded-full bg-purple-500 text-purple-200 w-8 h-8 font-black flex items-center justify-center">i</div>
                    <span class="font-medium">Si tienes alguna duda sobre cómo montar tu entorno para hacer la prueba, no dudes en <a class="font-bold" href="#contact">contactar con nosotros</a>. Te ayudaremos a ponerlo en marcha.</span>
                </div>
            </div>
        </div>
        <div>
            <div class="mt-16">
                <div class="sm:flex sm:items-baseline sm:justify-between">
                    <h2 id="architecture" class="text-2xl font-semibold tracking-tight font-display text-gray-900 sm:text-3xl">
                        Arquitectura
                    </h2>
                </div>
                <div>
                    <div class="mx-auto mt-4 bg-white border-b border-t border-gray-200 sm:border sm:rounded-lg overflow-hidden ">
                        <img class="mx-auto hidden sm:block" src="./images/architecture.svg" alt="Arquitectura">
                    </div>
                </div>
            </div>
            <div class="border-t border-gray-200 mt-8 pt-8 grid grid-cols-1 gap-y-6 lg:grid-cols-5 lg:gap-5">
                <div>
                    <h3 class="text-lg font-medium tracking-tight text-gray-900">
                        Base de datos
                    </h3>
                </div>
                <div class="col-span-4">
                    <div class="">
                        <p>
                            El contenedor docker <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">data-base</span> aloja un servidor de bases de datos MySQL.
                            En este servidor se encuentra una base de datos llamada <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">sixphere</span> que contiene
                            dos tablas: <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">orders</span> y <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">operations</span>.
                            En el siguiente diagrama entidad-relación se puede ver la definición de cada una de ellas:
                        </p>
                        <div class="mx-auto mt-4 bg-white border-b border-t border-gray-200 sm:border sm:rounded-lg overflow-hidden ">
                            <img class="mx-auto hidden sm:block" src="./images/ERD.svg" alt="Diagrama entidad-relación">
                        </div>
                    </div>
                </div>
            </div>
            <div class="border-t border-gray-200 mt-8 pt-8 grid grid-cols-1 gap-y-6 lg:grid-cols-5 lg:gap-5">
                <div>
                    <h3 class="text-lg font-medium tracking-tight text-gray-900">
                        Servidor <span class="italic">API rest</span>
                    </h3>
                </div>
                <div class="col-span-4">
                    <div class="">
                        <p>
                            En el contenedor <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">rest-api-server</span> corre una aplicación
                            <span class="italic">.Net Core</span> que expone un <span class="italic">endpoint</span> para obtener todas las órdenes del sistema. En
                            esta aplicación se encuentra la siguiente distribución de carpetas:
                        </p>
                        <div class="mx-auto my-4 p-4 bg-white text-gray-500 border-b font-mono border-t border-gray-200 sm:border sm:rounded-lg overflow-hidden">
                            <div class="my-0 leading-none">├── Controllers</div>
                            <div class="my-0 leading-none">│   └── OrdersController.cs</div>
                            <div class="my-0 leading-none">└── Models</div>
                            <pre class="my-0 leading-none">  ├── Order.cs</pre>
                            <pre class="my-0 leading-none">  └── OrdersContext.cs</pre>
                        </div>
                        <p>
                            Dentro de la carpeta <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">Controllers</span> se encuentra la clase
                            que define el <span class="italic">endpoint</span> de órdenes <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">/api/orders</span>.
                            En <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">Models</span> se encuentran la definición del modelo de órdenes
                            y la definición del contexto de base de datos para la entidad orden.
                        </p>
                    </div>
                </div>
            </div>
            <div class="border-t border-gray-200 mt-8 pt-8 grid grid-cols-1 gap-y-6 lg:grid-cols-5 lg:gap-5">
                <div>
                    <h3 class="text-lg font-medium tracking-tight text-gray-900">
                        Cliente Web
                    </h3>
                </div>
                <div class="col-span-4">
                    <div class="">
                        <div class="">
                            <p>
                                Dentro del contenedor <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">web-client</span> se ha creado una aplicación
                                <span class="italic">Laravel</span> para mostrar el listado de órdenes. Bajo la carpeta <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">app/Http/Controllers</span>
                                se encuentra el controlador que muestra esta lista <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">OrderControllers.php</span>.
                            </p>
                            <br/>
                            <p>
                                Este controlador renderiza la plantilla <span class="italic">Blade</span> que se encuentra en la ruta
                                <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">resources/views/orders/index.blade.php</span>.
                                El elemento principal de esta plantilla es el componente <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">order-list</span>
                                el cual es un componente <span class="italic">VueJS</span>, su definición se encuentra en <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">resources/js/components/OrdersList.vue</span>
                            </p>
                        </div>
                        <div class="rounded bg-indigo-200 p-4 mt-8">
                            <div class="text-sm text-indigo-500 flex items-center">
                                <div class="mr-4 rounded-full bg-indigo-500 text-indigo-200 w-8 h-8 font-black flex items-center justify-center">i</div>
                                <span class="font-medium">
                                    Si quieres que los cambios realizados en VueJS tengan efecto ejecuta el comando
                                    <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">npm run watch</span>
                                    dentro del contenedor <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">web-client</span>.
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="mt-16">
                    <div class="sm:flex sm:items-baseline sm:justify-between">
                        <h2 id="exercices" class="text-2xl font-semibold tracking-tight font-display text-gray-900 sm:text-3xl">
                            Ejercicios
                        </h2>
                    </div>
                </div>
                <div class="border-t border-gray-200 mt-8 pt-8 grid grid-cols-1 gap-y-6 lg:grid-cols-5 lg:gap-5">
                    <div>
                        <h3 class="text-lg font-medium tracking-tight text-gray-900">
                            Introducción
                        </h3>
                    </div>
                    <div class="col-span-4">
                        <div class="">
                            <p>
                                Para dar un poco más de contexto a los cambios que se pedirán en los siguientes ejercicios, imagina que se require realizar un nuevo desarrollo
                                que permita a los usuarios visualizar el listado de operaciones asociados a una orden. Para ello, es necesario implementar una integración entre
                                <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">web-client</span> y
                                <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">rest-api-server</span>.
                            </p>
                            <br>
                            <p>
                                Además, la pantalla de listado debe contar con una sección que implemente un buscador que ayude al usuario a encontrar operaciones por código
                                o descripción, así como un campo de filtrado por tipo de operación.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="border-t border-gray-200 mt-8 pt-8 grid grid-cols-1 gap-y-6 lg:grid-cols-5 lg:gap-5">
                    <div>
                        <h3 class="text-lg font-medium tracking-tight text-gray-900">
                            #1 Servicio de operaciones
                        </h3>
                    </div>
                    <div class="col-span-4">
                        <div class="">
                            <p>
                                Para acceder a los datos de operaciones es necesario que el servidor <span class="italic">API Rest</span> exponga un nuevo <span class="italic">endpoint</span>.
                                Desarrolla un nuevo endpoint con la siguiente nomenclatura <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">/api/orders/{id}/operations</span>
                                que devuelva en formato <span class="italic">JSON</span> todas las operaciones de la base de datos asociadas a la orden con el identificador
                                <span class="mx-auto rounded px-2 py-1 bg-gray-200 font-mono text-gray-500">id</span>.
                            </p>
                            <div class="rounded bg-indigo-200 p-4 mt-8">
                                <div class="text-sm text-indigo-500 flex items-center">
                                    <div class="mr-4 rounded-full bg-indigo-500 text-indigo-200 w-8 h-8 font-black flex items-center justify-center">i</div>
                                    <span class="font-medium">
                                        No olvides respetar el <a class="font-bold" href="#architecture">diagrama de arquitectura</a> propuesto. Para mantener la complejidad
                                        de la prueba sólo el servidor <span class="italic">API Rest</span> debe tener acceso a la base de datos.
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border-t border-gray-200 mt-8 pt-8 grid grid-cols-1 gap-y-6 lg:grid-cols-5 lg:gap-5">
                    <div>
                        <h3 class="text-lg font-medium tracking-tight text-gray-900">
                            #2 Listado de operaciones
                        </h3>
                    </div>
                    <div class="col-span-4">
                        <div class="">
                            <p>
                                Desarrolla un controlador <span class="italic">Laravel</span> que, dado el id de una orden,
                                relice una petición al servidor <span class="italic">API Rest</span> y devuelva los datos obtenidos.

                                Luego crea una vista <span class="italic">Blade</span> que contenga un componente <span class="italic">Vue</span>
                                que muestre al usuario la lista de operaciones asociadas a una orden. Los elementos de la lista
                                deben contener los siguientes campos:
                            </p>
                            <ul class="ml-4 list-inside list-disc">
                                <li>Código de operación</li>
                                <li>Descripción</li>
                                <li>Tipo</li>
                                <li>Fecha de Creación</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="border-t border-gray-200 mt-8 pt-8 grid grid-cols-1 gap-y-6 lg:grid-cols-5 lg:gap-5">
                    <div>
                        <h3 class="text-lg font-medium tracking-tight text-gray-900">
                            #3 Filtrado de operaciones
                        </h3>
                    </div>
                    <div class="col-span-4">
                        <div class="">
                            <p>
                                Añade a la vista dos filtros que ayuden al usuario a encontrar las operaciones
                                (Puedes hacerlo modificando el componente <span class="italic">Vue</span> del ejercicio #2):
                            </p>
                            <ul class="ml-4 list-inside list-disc">
                                <li>
                                    Buscador de términos - Implementa un campo de texto que permita filtrar las operaciones introduciendo
                                    términos que sean parte del código o la descripción.
                                </li>
                                <li>
                                    Filtro de tipo - Implementa un seleccionable que contenga todos los tipos que aparecen en la lista de
                                    operaciones y que al seleccionar uno de ellos filtre el resultado por el campo Tipo.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="rounded bg-purple-200 p-4 mt-8">
                    <div class="text-sm text-purple-500 flex items-center">
                        <div class="mr-4 rounded-full bg-purple-500 text-purple-200 w-8 h-8 font-black flex items-center justify-center">i</div>
                        <span class="font-medium">
                            Siéntete libre de desarrollar los ejercicios en el orden que quieras.
                        </span>
                    </div>
                </div>
            </div>
            <div>
                <div class="mt-16">
                    <div class="sm:flex sm:items-baseline sm:justify-between">
                        <h2 id="byebye" class="text-2xl font-semibold tracking-tight font-display text-gray-900 sm:text-3xl">
                            Para finalizar
                        </h2>
                    </div>
                </div>
                <div class="border-t border-gray-200 mt-8 pt-8 grid grid-cols-1 gap-y-6 lg:grid-cols-5 lg:gap-5">
                    <div>
                        <h3 class="text-lg font-medium tracking-tight text-gray-900">
                            Una cosa más
                        </h3>
                    </div>
                    <div class="col-span-4">
                        <div class="">
                            <p class="border-l-4 border-gray-400 py-4 pl-4 text-gray-400 mb-8">
                                Leave the campground cleaner than you found it.
                            </p>
                            <p>
                                Si lo ves necesario, siéntete totalmente libre de modificar a tu gusto el código que te damos.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="border-t border-gray-200 mt-8 pt-8 grid grid-cols-1 gap-y-6 lg:grid-cols-5 lg:gap-5">
                    <div>
                        <h3 class="text-lg font-medium tracking-tight text-gray-900">
                            Código
                        </h3>
                    </div>
                    <div class="col-span-4">
                        <div class="">
                            <p>
                                Antes de finalizar, si has llegado hasta aquí y has completado los ejercicios propuestos (o parte de ellos),
                                no olvides hacernos llegar el código. Te proponemos que lo subas con tu cuenta personal de tu proveedor git y que nos des acceso, así podremos verificar el resultado.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="mt-16">
                    <div class="sm:flex sm:items-baseline sm:justify-between">
                        <h2 id="contact" class="text-2xl font-semibold tracking-tight font-display text-gray-900 sm:text-3xl">
                            Contacto
                        </h2>
                    </div>
                </div>
                <div class="border-t border-gray-200 mt-8 pt-8 grid grid-cols-1 gap-y-6 lg:grid-cols-5 lg:gap-5">
                    <div class="col-span-5">
                        <div class="flex justify-center">
                            <div class="bg-white rounded-xl p-8 w-80">
                                <img class="w-32 h-32 rounded-full mx-auto" src="https://sixphere.com/wp-content/uploads/2019/06/alvaro.jpg" alt="" width="384" height="512">
                                <div class="pt-6 text-center space-y-4">
                                    <figcaption class="font-medium">
                                        <div>
                                            <div style="color: #543855;">
                                                Álvaro Fernández García
                                            </div>
                                            <div style="color: #ffa327;">
                                                DevOps
                                            </div>
                                        </div>
                                        <div class="text-sm mt-2">
                                            <div class="text-gray-500">
                                                alvaro.fernandez.garcia@sixphere.com
                                            </div>
                                            <div class="text-gray-500">
                                                +34 636 183 612
                                            </div>
                                        </div>
                                    </figcaption>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
