@extends('layout')

@section('main-content')
    <div id="app" class="container mx-auto">
        <order-list></order-list>
    </div>
@endsection
