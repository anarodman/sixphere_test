<footer class="bg-gray-300 py-8 mt-16">
    <div class="flex items-center justify-center font-medium">
        <div class="text-white">SIXPHERE TECHNOLOGIES 2021 | <span class="text-yellow-500">#WeAreCodelovers</span></div>
    </div>
</footer>
