@extends('layout')

@section('main-content')
    <div id="app" class="container mx-auto">
        <operation-list></operation-list>
    </div>
@endsection
