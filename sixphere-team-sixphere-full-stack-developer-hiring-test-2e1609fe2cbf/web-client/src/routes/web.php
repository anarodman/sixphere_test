<?php

use App\Http\Controllers\OrdersController;
use App\Http\Controllers\OperationsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/operations/{id}', "OperationsController@get1");

Route::resources([
    'orders' => OrdersController::class
]);

Route::resources([
    'operations' => OperationsController::class
]);
